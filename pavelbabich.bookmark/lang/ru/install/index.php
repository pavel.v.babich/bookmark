<?
$MESS["BOOKMARK_MAIN_MODULE_INSTALL_NAME"] = "Сервис общедоступных закладок";
$MESS["BOOKMARK_MAIN_MODULE_INSTALL_DESCR"] = "Данный модуль предназначен публикации/редактирования общедоступных закладок";
$MESS["BOOKMARK_INSTALL_TITLE"] = "Установка модуля \"Сервис общедоступных закладок\"";
$MESS["BOOKMARK_COPY_FOLDER"] = "Папка в которую будет установлен модуль";
$MESS["BOOKMARK_MOD_INSTALL"] = "Установить модуль";
?>