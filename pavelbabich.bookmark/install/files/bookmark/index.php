<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("bookmark");
?>
<?$APPLICATION->IncludeComponent("bitrix:bookmark", "", [
    "HL_IBLOCK_ID" => COption::GetOptionString("pavelbabich.bookmark", "HL_ID"),
    "NEWS_COUNT" => "3",
    "SORT_BY" => "UF_CREATE_DATE",
    "SORT_ORDER" => "DESC",
    "SEF_FOLDER" => "/bookmark/",
    "SEF_URL_TEMPLATES" => [
        "news" => "",
        "add" => "add/",
        "detail" => "#ELEMENT_CODE#/",
        "edit" => "#ELEMENT_CODE#/edit/"
      ],
    ]
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>