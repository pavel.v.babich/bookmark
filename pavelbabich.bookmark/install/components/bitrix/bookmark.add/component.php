<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule('pavelbabich.bookmark');
$HL = new BOOKMARKModuleMain;
$folder = COption::GetOptionString("pavelbabich.bookmark", "FOLDER");

if($_REQUEST["save"]){
    $arFields = $_REQUEST["fields"];
    $arPage = $HL->cURL($arFields["UF_URL"]);
    if($arPage["HEADERS"]["http_code"] == "200"){
        $arDocData = $HL->getDocData($arPage["RESULT"]);
        if($arDocData["favicon"]){
            if(substr($arDocData["favicon"],0,1)=="/" && substr($arDocData["favicon"],0,2)!="//"){
                $domain_validation = "/(?:http(s)?:\/\/)?(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+(?:[a-z]{2,63}|[a-z0-9]{2,59})?(?:\/)+$/i";
                preg_match($domain_validation, $arFields["UF_URL"], $arDomain);
                if ($arDomain["0"]) {
                    $arDocData["favicon"] = $arDomain["0"].$arDocData["favicon"];
                }
            }
            $arFavicon = CFile::MakeFileArray($arDocData["favicon"]);
            $arFields["UF_FAVICON"] = $arFavicon;
            $arFields["UF_META_DESCRIPTION"] = $arDocData["description"];
            $arFields["UF_META_KEYWORDS"] = $arDocData["keywords"];
            $CODE = $HL->SaveBookmarkElement($arFields);
            if($CODE){
                LocalRedirect("/".$folder."/".$CODE."/");
            }else{
                $arResult["errors"] = "Ошибка сохранения!";
            }
        }else{
            $arResult["errors"] = "Favicon не обнаружен";
        }
    }else{
        $arResult["errors"] = "Ошибка открытия url страницы!";
    }
}

global $APPLICATION;
$APPLICATION->AddChainItem("Новая закладка", "/bookmark/add/");
    
$this->includeComponentTemplate($componentPage);
?>