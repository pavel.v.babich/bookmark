<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$CACHE_ID = md5(SITE_ID."||".$APPLICATION->GetCurPage()."|".serialize($arParams));
$cache = new CPHPCache;
if ($cache->InitCache(36000000, $CACHE_ID, $APPLICATION->GetCurPage())){
   $arResult = $cache->GetVars();
}elseif($cache->StartDataCache()){
    global $CACHE_MANAGER;
    $CACHE_MANAGER->StartTagCache($APPLICATION->GetCurPage());
    
    CModule::IncludeModule('pavelbabich.bookmark');
    $HL = new BOOKMARKModuleMain;
    $arResult = $HL->GetBookmarkElement($arParams["ELEMENT_CODE"]);
    
    if($arResult["ID"]){
        $CACHE_MANAGER->RegisterTag("HL_BELEMENT_".$arResult["ID"]);
        $CACHE_MANAGER->EndTagCache();
        $cache->EndDataCache($arResult);
    }
}

if($arResult["UF_TITLE"] &&
  $arResult["UF_CODE"])
{
    global $APPLICATION;
    $APPLICATION->AddChainItem($arResult["UF_TITLE"], "/bookmark/".$arResult["UF_CODE"]."/");
}

$this->includeComponentTemplate($componentPage);
?>