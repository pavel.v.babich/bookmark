<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule('pavelbabich.bookmark');
$HL = new BOOKMARKModuleMain;
$arResult = $HL->GetBookmarkElement($arParams["ELEMENT_CODE"]);
$folder = COption::GetOptionString("pavelbabich.bookmark", "FOLDER");

if($arResult){
    $password = htmlspecialchars($_REQUEST["password"]);
    if(!$password)$arResult["errors"] = "Требуется ввести пароль!";
    elseif($password!=$arResult["UF_PASSWORD"])$arResult["errors"] = "Не верный пароль!";
    else $arResult["access"] = true;
    
    if($arResult["access"] && ($_REQUEST["save"] || $_REQUEST["del"]=="Y")){
        if($_REQUEST["del"]=="Y")$arFields = array("UF_ACTIVE"=>0);
            else $arFields = $_REQUEST["fields"];
        if($HL->UpdateBookmarkElement($arResult["ID"], $arFields)) LocalRedirect("/".$folder."/");
            else $arResult["errors"] = "Ошибка сохранения!";
    }

    if($arResult["UF_TITLE"] &&
      $arResult["UF_CODE"])
    {
        global $APPLICATION;
        $APPLICATION->AddChainItem($arResult["UF_TITLE"], "/bookmark/".$arResult["UF_CODE"]."/");
    }
    
    $this->includeComponentTemplate($componentPage);
}else{
    echo "<b style='color:red'>Элемент не найден!</b>";
}
?>