<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "BOOKMARK",
	"DESCRIPTION" => "BOOKMARK",
	"ICON" => "/images/bookmark_all.gif",
	"COMPLEX" => "Y",
	"PATH" => array(
		"ID" => "bookmark",
		"CHILD" => array(
			"ID" => "bookmark",
			"NAME" => "BOOKMARK",
			"SORT" => 10,
			"CHILD" => array(
				"ID" => "bookmark_cmpx",
			),
		),
	),
);

?>