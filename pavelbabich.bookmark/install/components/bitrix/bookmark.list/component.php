<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$CACHE_ID = md5(SITE_ID."||".$APPLICATION->GetCurPage()."|".serialize($arParams));
$cache = new CPHPCache;
if (false && $cache->InitCache(36000000, $CACHE_ID, $APPLICATION->GetCurPage())){
   $arResult = $cache->GetVars();
}elseif($cache->StartDataCache()){
    global $CACHE_MANAGER;
    $CACHE_MANAGER->StartTagCache($APPLICATION->GetCurPage());
    
    CModule::IncludeModule('pavelbabich.bookmark');
    $HL = new BOOKMARKModuleMain;

    $arResult = $HL->GetBookmarkList($arParams["NEWS_COUNT"], $arParams["SORT_BY"], $arParams["SORT_ORDER"], $arParams["NEWS_COUNT"]);
    
    $CACHE_MANAGER->RegisterTag("HL_BKMRK");
    $CACHE_MANAGER->EndTagCache();
    $cache->EndDataCache($arResult);
}

$this->includeComponentTemplate($componentPage);
?>