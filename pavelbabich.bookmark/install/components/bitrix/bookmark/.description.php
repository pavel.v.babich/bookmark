<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "TELEGRAPH",
	"DESCRIPTION" => "TELEGRAPH",
	"ICON" => "/images/telegraph_all.gif",
	"COMPLEX" => "Y",
	"PATH" => array(
		"ID" => "telegraph",
		"CHILD" => array(
			"ID" => "telegraph",
			"NAME" => "TELEGRAPH",
			"SORT" => 10,
			"CHILD" => array(
				"ID" => "telegraph_cmpx",
			),
		),
	),
);

?>