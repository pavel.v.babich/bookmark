<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<div>
    <?if($arResult["errors"]){?>
        <div class="errors_block"><?=$arResult["errors"]?></div>
    <?}?>
    <form method="post">
        <fieldset>
            <label for="title">Заголовок</label>
            <input id="title"
                size="60"
                name="fields[UF_TITLE]"
                value="<?=$_REQUEST["fields"]["UF_TITLE"]?htmlspecialchars($_REQUEST["fields"]["UF_TITLE"]):$arResult["UF_TITLE"]?>"
              >
        </fieldset>
        <fieldset>
            <label for="url">URL страницы</label>
            <input id="url"
                size="60"
                name="fields[UF_URL]"
                value="<?=$_REQUEST["fields"]["UF_URL"]?htmlspecialchars($_REQUEST["fields"]["UF_URL"]):$arResult["UF_URL"]?>"
              >
        </fieldset>
        <fieldset>
            <label for="password">Пароль</label>
            <input id="password"
                size="60"
                name="fields[UF_PASSWORD]"
                value="<?=$_REQUEST["fields"]["UF_PASSWORD"]?htmlspecialchars($_REQUEST["fields"]["UF_PASSWORD"]):$arResult["UF_PASSWORD"]?>"
              >
        </fieldset>
        <br>
        <input type="submit" name="save" value="Сохранить">
    </form>
</div>