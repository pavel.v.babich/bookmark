<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<?if(!$arResult){?>
    <b style='color:red'>Элемент не найден!</b>
<?}else{?>
    <div>
        <h1><?=$arResult["UF_TITLE"]?></h1>
        <span><?=$arResult["UF_CREATE_DATE"]?></span>
        <?if($arResult["UF_FAVICON"]){?>
          <br><img src="<?=CFile::GetPath($arResult["UF_FAVICON"])?>" style="width:50px;height:50px;"><br>
        <?}?>
        <p><b>URL:</b> <?=$arResult["UF_URL"]?></p>
        <p><b>Description:</b> <?=$arResult["UF_META_DESCRIPTION"]?></p>
        <p><b>Keywords:</b> <?=$arResult["UF_META_KEYWORDS"]?></p>
        <hr>
        <a href="edit/" class="edit_article">Редактировать</a><br><br>
        <a href="edit/?del=Y" class="delete_article">удалить</a>
    </div>
<?}?>