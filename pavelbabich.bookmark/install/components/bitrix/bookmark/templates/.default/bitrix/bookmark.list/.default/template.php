<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<div>
    <a href="add/" class="add_article">Добавить</a>
    <?if(!empty($arResult["ITEMS"])):?>
    <?foreach($arResult["ITEMS"] as $Item){?>
        <div>
            <h3><a href="<?=$Item["UF_CODE"]?>/"><?=$Item["UF_TITLE"]?></a></h3>
            <span><?=$Item["UF_CREATE_DATE"]?></span>
            <?if($Item["UF_FAVICON"]){?>
              <br><img src="<?=CFile::GetPath($Item["UF_FAVICON"])?>" style="width:50px;height:50px;"><br>
            <?}?>
            <p><b>URL:</b> <?=$Item["UF_URL"]?></p>
            <a href="<?=$Item["UF_CODE"]?>/edit/" class="edit_article">Редактировать</a>
            <a href="<?=$Item["UF_CODE"]?>/edit/?del=Y" class="edit_article">Удалить</a>
            <hr>
        </div>
    <?}?>
    <?endif;?>
</div>

<?
$APPLICATION->IncludeComponent(
   "bitrix:main.pagenavigation",
   "",
   array(
      "NAV_OBJECT" => $arResult["NAV"],
      "SHOW_ALL" => "N",
      "SEF_MODE" => "Y",
   ),
   false
);
?>