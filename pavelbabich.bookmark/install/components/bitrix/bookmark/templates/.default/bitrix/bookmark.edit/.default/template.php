<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<div>
<?if(!$arResult["access"]){?>
    <h1><?=$arResult["UF_TITLE"]?></h1>
    <span><?=$arResult["UF_CREATE_DATE"]?></span>
    <?if($arResult["UF_FAVICON"]){?>
      <br><img src="<?=CFile::GetPath($arResult["UF_FAVICON"])?>" style="width:50px;height:50px;"><br>
    <?}?>
    <p><b>URL:</b> <?=$arResult["UF_URL"]?></p>
    <p><b>Description:</b> <?=$arResult["UF_META_DESCRIPTION"]?></p>
    <p><b>Keywords:</b> <?=$arResult["UF_META_KEYWORDS"]?></p>
    <div class="errors_block"><?=$arResult["errors"]?></div>
    <form method="post">
        <fieldset>
            <label for="password"></label>
            <input id="password" name="password" value="" autocomplete="off">
        </fieldset>
        <br>
        <input type="submit" name="edit" value="<?=$_REQUEST["del"]=="Y"?"Удалить":"Редактировать"?>">
    </form>
<?}else{?>
    <?if($arResult["errors"]){?>
        <div class="errors_block"><?=$arResult["errors"]?></div>
    <?}?>
    <form method="post">
        <input type="hidden" name="password" value="<?=htmlspecialchars($_REQUEST["password"])?>">
        <fieldset>
            <label for="UF_TITLE">Заголовок</label>
            <input id="UF_TITLE"
                size="80"
                name="fields[UF_TITLE]"
                value="<?=$_REQUEST["fields"]["UF_TITLE"]?htmlspecialchars($_REQUEST["fields"]["UF_TITLE"]):$arResult["UF_TITLE"]?>"
              >
        </fieldset>
        <fieldset>
            <label for="UF_CODE">Код</label>
            <input id="UF_CODE"
                size="80"
                name="fields[UF_CODE]"
                value="<?=$_REQUEST["fields"]["UF_CODE"]?htmlspecialchars($_REQUEST["fields"]["UF_CODE"]):$arResult["UF_CODE"]?>"
              >
        </fieldset>
        <fieldset>
            <label for="UF_META_DESCRIPTION">Description</label>
            <input id="UF_META_DESCRIPTION"
                size="80"
                name="fields[UF_META_DESCRIPTION]"
                value="<?=$_REQUEST["fields"]["UF_META_DESCRIPTION"]?htmlspecialchars($_REQUEST["fields"]["UF_META_DESCRIPTION"]):$arResult["UF_META_DESCRIPTION"]?>"
              >
        </fieldset>
        <fieldset>
            <label for="UF_META_KEYWORDS">Keywords</label>
            <input id="UF_META_KEYWORDS"
                size="80"
                name="fields[UF_META_KEYWORDS]"
                value="<?=$_REQUEST["fields"]["UF_META_KEYWORDS"]?htmlspecialchars($_REQUEST["fields"]["UF_META_KEYWORDS"]):$arResult["UF_META_KEYWORDS"]?>"
              >
        </fieldset>
        <br>
        <input type="submit" name="save" value="Сохранить">
    </form>
<?}?>
</div>