<?
use Bitrix\Main\EventManager;
use Bitrix\Highloadblock as HL;
IncludeModuleLangFile(__FILE__, 'ru');
if(class_exists("pavelbabich_bookmark")) return;

Class pavelbabich_bookmark extends CModule
{
	var $MODULE_ID = "pavelbabich.bookmark";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;

	function pavelbabich_bookmark()
	{
		$arModuleVersion = [];

		$path = str_replace("\\", "/", __DIR__);
		include($path . "/version.php");

		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}

		$this->MODULE_NAME = GetMessage("BOOKMARK_MAIN_MODULE_INSTALL_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("BOOKMARK_MAIN_MODULE_INSTALL_DESCR");
		$this->PARTNER_NAME = "Pavel Babich";
		$this->PARTNER_URI = "https://vk.com/skif_p";
	}

	function DoInstall()
	{
        global $DB, $APPLICATION, $step;
        
        $step = IntVal($step);
        
        if($step < 2 && !$_REQUEST["public_dir"])
            $APPLICATION->IncludeAdminFile(
              GetMessage("BOOKMARK_INSTALL_TITLE"),
              $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/pavelbabich.bookmark/install/step1.php"
            );
        elseif($step == 2){
            CModule::IncludeModule("iblock");
            CModule::IncludeModule('highloadblock');
            $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::Add(array(
                'NAME' => 'Bookmark',
                'TABLE_NAME' => 'bookmark',
            ));

            if ($hlblock->isSuccess()){
                $ID = $hlblock->getId();
                $oUserTypeEntity    = new CUserTypeEntity();
                $arTFields = [
                    0 => [
                        "CODE" => "TITLE", // "AUTHOR_NAME",
                        "TYPE" => "string",
                        "NAME" => [
                            "ru"    => "Заголовок страницы ",
                            "en"    => "Page title",
                            ]
                        ],
                    1 => [
                        "CODE" => "CREATE_DATE",
                        "TYPE" => "datetime",
                        "NAME" => [
                            "ru"    => "Дата создания",
                            "en"    => "Create date",
                            ]
                        ],
                    2 => [
                        "CODE" => "URL", // "BODY",
                        "TYPE" => "string",
                        "NAME" => [
                            "ru"    => "URL страницы",
                            "en"    => "Page URL",
                            ]
                        ],
                    3 => [
                        "CODE" => "FAVICON",
                        "TYPE" => "file",
                        "NAME" => [
                            "ru"    => "Favicon",
                            "en"    => "Favicon",
                            ]
                        ],
                    4 => [
                        "CODE" => "CODE",
                        "TYPE" => "string",
                        "NAME" => [
                            "ru"    => "Символьный код",
                            "en"    => "Code",
                            ]
                        ],
                    5 => [
                        "CODE" => "ACTIVE",
                        "TYPE" => "boolean",
                        "NAME" => [
                            "ru"    => "Активность",
                            "en"    => "Active",
                            ]
                        ],
                    6 => [
                        "CODE" => "META_DESCRIPTION",
                        "TYPE" => "string",
                        "NAME" => [
                            "ru"    => "META Description",
                            "en"    => "META Description",
                            ]
                        ],
                    7 => [
                        "CODE" => "META_KEYWORDS",
                        "TYPE" => "string",
                        "NAME" => [
                            "ru"    => "META Keywords",
                            "en"    => "META Keywords",
                            ]
                        ],
                    8 => [
                        "CODE" => "PASSWORD",
                        "TYPE" => "string",
                        "NAME" => [
                            "ru"    => "Пароль",
                            "en"    => "PASSWORD",
                            ]
                        ],
                ];
                foreach($arTFields as $TField){
                    $aUserFields    = [
                        'ENTITY_ID'         => 'HLBLOCK_'.$ID,
                        'FIELD_NAME'        => 'UF_'.$TField["CODE"],
                        'USER_TYPE_ID'      => $TField["TYPE"],
                        'MULTIPLE'          => 'N',
                        'MANDATORY'         => 'N',
                        'SHOW_FILTER'       => 'I',
                        'EDIT_FORM_LABEL'   => $TField["NAME"]
                    ];
                    $iUserFieldId   = $oUserTypeEntity->Add( $aUserFields );
                }
            }
            
            COption::SetOptionString("pavelbabich.bookmark", "FOLDER", $_REQUEST["public_dir"]);
            COption::SetOptionString("pavelbabich.bookmark", "HL_ID", $ID);
            
            CopyDirFiles(__DIR__ ."/components/bitrix/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/components/bitrix/", true, true);
            CopyDirFiles(__DIR__ ."/files/bookmark/", $_SERVER["DOCUMENT_ROOT"]."/".$_REQUEST["public_dir"]."/", true, true);
            
            CUrlRewriter::Add([
                  "SITE_ID" => "s1",
                  "CONDITION" => "#^/".$_REQUEST["public_dir"]."/#",
                  "ID" => "bitrix:bookmark",
                  "PATH" => "/".$_REQUEST["public_dir"]."/index.php",
                  "RULE" => ""
               ]
            );
            
            RegisterModule($this->MODULE_ID);
                        
            /*
            EventManager::getInstance()->registerEventHandler(
                "",
                "BookmarkOnBeforeUpdate",
                $this->MODULE_ID,
                'pavelbabich\\bookmark\\BOOKMARKModuleMain',
                "BookmarkBeforeUpdate"
            );
            */
        
            $APPLICATION->IncludeAdminFile(GetMessage("BOOKMARK_INSTALL_TITLE"),
                    $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/pavelbabich.bookmark/install/step2.php");
        }
	}

	function DoUninstall()
	{
        CModule::IncludeModule('highloadblock');
        $eventManager = \Bitrix\Main\EventManager::getInstance(); 
        $eventManager->unRegisterEventHandler(
                "",
                "BookmarkOnBeforeUpdate",
                $this->MODULE_ID,
                'pavelbabich\\bookmark\\BOOKMARKModuleMain',
                'BookmarkBeforeUpdate'
            );
        \Bitrix\Highloadblock\HighloadBlockTable::Delete(COption::GetOptionString("pavelbabich.bookmark", "HL_ID"));
        //DeleteDirFilesEx("/bitrix/admin/pavelbabich_bookmark.php");
		UnRegisterModule($this->MODULE_ID);
        CUrlRewriter::Delete(array("ID" => "bitrix:bookmark"));
		return true;
	}
}

?>