<?
use Bitrix\Highloadblock as HL;
IncludeModuleLangFile(__FILE__, "ru");
CModule::IncludeModule('highloadblock');

$hlblock = HL\HighloadBlockTable::getById(COption::GetOptionString("pavelbabich.bookmark", "HL_ID"))->fetch();
$entity = HL\HighloadBlockTable::compileEntity( $hlblock );
$entityClass = $entity->getDataClass();

class BOOKMARKModuleMain extends \BookmarkTable{
    
    public static function getEntity()
    {
        return static::$entity['\\BookmarkTable'];
    }
    
    public static function GetBookmarkElement($CODE){
        $rsData = static::getList(array(
           'select' => array('*'),
           'filter' => array("UF_ACTIVE" => 1, "UF_CODE"=>$CODE)
        ));
        return $rsData->Fetch();
    }
    
    public static function GetBookmarkList($newsCount, $sortBy="UF_CREATE_DATE", $sortOrder="DESC", $count = 3){
        $maxRecord = static::getList([
           'select' => array('ID'),
           'filter' => array("UF_ACTIVE" => 1),
        ])->getSelectedRowsCount();

        $nav = new \Bitrix\Main\UI\PageNavigation("nav-more-news");
        $nav->allowAllRecords(true)
           ->setPageSize( $count )
           ->initFromUri();
        $nav->setRecordCount($maxRecord);

        $rsData = static::getList(array(
           'select' => array('*'),
           'filter' => array("UF_ACTIVE" => 1),
           'order' => array($sortBy => $sortOrder),
           'limit' => $nav->getLimit(),
           'offset' => $nav->getOffset(),
        ));

        while($data = $rsData->Fetch()){
            $arData["ITEMS"][] = $data;
        }
        $arData["NAV"] = $nav;
        return $arData;
    }
    
    public static function UpdateBookmarkElement($ID, $arFields){
        foreach($arFields as &$field) $field = htmlspecialchars($field);
        if(static::update($ID, $arFields)){
            global $CACHE_MANAGER;
            $CACHE_MANAGER->ClearByTag("HL_BKMRK");
            $CACHE_MANAGER->ClearByTag("HL_BELEMENT_".$ID);
            return true;
        }else return false;
    }
    
    public static function SaveBookmarkElement($arFields){
        foreach($arFields as &$field){
          if(is_string($field))$field = htmlspecialchars($field);
        }
        unset($field);

        $arCurrent = static::getList(array(
           'select' => array('ID'),
           'filter' => array("UF_ACTIVE" => 1, "UF_URL" => $arFields["UF_URL"]),
        ))->fetch();

        if($arCurrent["ID"]){
            return false;
        }

        $arFields["UF_CODE"] = Cutil::translit($arFields["UF_TITLE"],"ru");
        $arFields["UF_ACTIVE"] = 1;
        $arFields["UF_CREATE_DATE"] = date("d.m.Y H:i:s");
        $result = static::add($arFields);
        if($result->isSuccess()){
            global $CACHE_MANAGER;
            $CACHE_MANAGER->ClearByTag("HL_BKMRK");
            return $arFields["UF_CODE"];
        }else{
            // implode(', ', $result->getErrors());
            return false;
        }
    }
    
    public static function BookmarkBeforeUpdate(\Bitrix\Main\Entity\Event $event)
    {
        global $CACHE_MANAGER;
        $param = $event->getParameters();
        $CACHE_MANAGER->ClearByTag("HL_BELEMENT_".$param["id"]["ID"]);
    }

    public static function getDocFavicon($docHTML){
        libxml_use_internal_errors(true);
        $doc = new DOMDocument();
        $doc->strictErrorChecking = FALSE;
        $doc->loadHTML($docHTML);
        $xml = simplexml_import_dom($doc);
        $arr = $xml->xpath('//link[@rel="shortcut icon"]');
        if($arr[0]['href']){
            return (string)$arr[0]['href'];
        }else{
            return false;
        }
    }

    public static function getDocData($docHTML){
        libxml_use_internal_errors(true);
        $doc = new DOMDocument();
        $doc->strictErrorChecking = FALSE;
        $doc->loadHTML($docHTML);
        $xml = simplexml_import_dom($doc);
        $arrFavicon = $xml->xpath('//link[@rel="shortcut icon"]');
        $arrFavicon2 = $xml->xpath('//link[@rel="icon"]');
        $arrDescription = $xml->xpath('//meta[@name="description"]');
        $arrKeyword = $xml->xpath('//meta[@name="keywords"]');
        $arDocData = [
            "favicon" => (string)$arrFavicon[0]['href']?(string)$arrFavicon[0]['href']:(string)$arrFavicon2[0]['href'],
            "description" => (string)$arrDescription[0]['content'],
            "keywords" => (string)$arrKeyword[0]['content'],
          ];

        return $arDocData;
    }

    public static function cURL($url, $p=[], $METHOD = 'POST'){
        $ch =  curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($ch, CURLOPT_REFERER, 'http://'.$_SERVER["SERVER_NAME"].'/');
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLINFO_HEADER_OUT, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        if($p && $METHOD == "POST_JSON"){
          curl_setopt($ch, CURLOPT_URL, $url);
          $dataJson = json_encode($p);
          curl_setopt($ch, CURLOPT_HTTPHEADER, [
                "Content-Type: application/json"
              ]);
          curl_setopt($ch, CURLOPT_POST, TRUE);
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
          curl_setopt($ch, CURLOPT_POSTFIELDS, $dataJson);        
        }elseif($p && $METHOD == 'POST'){
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: application/x-www-form-urlencoded"]);
          curl_setopt($ch, CURLOPT_POST, TRUE);
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
          curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($p));
        }elseif($p && $METHOD == 'PUT'){
          curl_setopt($ch, CURLOPT_URL, $url);
          $dataJson = json_encode($p);
          curl_setopt($ch, CURLOPT_HTTPHEADER, [
              "Content-Type: application/json",
              "Content-Length: ".strlen($dataJson)
            ]);
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
          curl_setopt($ch, CURLOPT_POSTFIELDS, $dataJson);
        }else{
          if($p){
            curl_setopt($ch, CURLOPT_URL, $url."?".http_build_query($p));
          }else{
            curl_setopt($ch, CURLOPT_URL, $url);
          }
          curl_setopt($ch, CURLOPT_HTTPHEADER, [
              "Content-Type: application/json",
            ]);
        }
        $result = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        if ($result){
            return array("RESULT" => $result, "HEADERS" => $info);
        }else{
            return '';
        }
    }
}
?>